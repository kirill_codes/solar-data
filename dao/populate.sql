
create table solar_data (
  id uuid not null default uuid_generate_v4(),
  panel_id uuid not null,
  date timestamp not null,
  latitude real not null,
  longitude real not null,
  temperature numeric not null,
  irradiance numeric not null,
  orientation numeric not null,
  power_output numeric not null
);
