
import { Pool } from "pg";
import { from } from "pg-copy-streams";
import fs from "fs";
import path from "node:path";
import { pipeline } from "node:stream/promises";

// using postgres to persist data
// hosting database on elephantsql
const {
  PG_CONNECTION_STRING = "postgres://localhost/postgres",
} = process.env;
// saved in .envrc -> `direnv allow` with insert secret in the env

const pool = new Pool({ connectionString: PG_CONNECTION_STRING, });

export async function queryDatabase (
  query: string, values?: Array<unknown>
) {
  const client = await pool.connect();

  try {
    const result = await client.query(query, values);

    await client.query("COMMIT");

    return result;
  } catch ( error: unknown ) {
    await client.query("ROLLBACK");
    throw error;
  } finally {
    client.release();
  }
}

export async function populateDatabase ( ) {
  const query = `copy solar_data (
    panel_id, date, latitude, longitude,
    temperature, irradiance,orientation, power_output
  ) from stdin delimiter ',' csv header;`;
  const client = await pool.connect();

  try {
    const ingestStream = client.query(from(query));
    const sourceStream = fs.createReadStream(
      path.join(__dirname, "./data.csv")
    );

    await pipeline(sourceStream, ingestStream);
  } catch ( e: unknown ) {
    return { ok: false, e, };
  } finally {
    client.release();
  }

  return true;
}
