import { readFileSync } from "fs";
import { withJm } from "@cran/gql.jm";
import { composeExecutableSchema, withCoreFeatures } from "@cran/gql.core";

export function getSchema ( ) {
  return composeExecutableSchema({
    typeDefs: readFileSync(
      `${__dirname}/../dao/schema.gql`, "utf-8"
    ),
    plugins: [
      ...withCoreFeatures({ }),
      ...withJm({ resolverOptions: { dialect: "pg", }, }),
    ],
  });
}
