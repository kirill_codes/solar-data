/* eslint-disable require-atomic-updates */
/* eslint-disable no-console */
/* eslint-disable max-lines-per-function */
import { Server as ApolloServer } from "@cran/gql.server";
import { Server as KoaServer } from "@cran/koa.core";
import Router from "@koa/router";
import bodyParser from "koa-bodyparser";
import format from "pg-format";
import { getSchema } from "../dao/schema";
import { populateDatabase, queryDatabase } from "../dao";

const PORT = process.env.PORT || 5555;

(async function main ( ) {
  const app = new KoaServer();
  const router = new Router({ methods: [ "POST", ], });

  const apollo = new ApolloServer({
    plugins: [
      ApolloServer.createDrainPlugin(app.getServer()),
    ],
    schema: getSchema(),
    context ( { ctx, } ) {
      return {
        ...ctx,
        async query ( sql: string ) {
          return queryDatabase(sql);
        },
        format ( ...args: Parameters<typeof format> ) {
          return format(...args);
        },
      };
    },
  });

  router.post("/populate", async function populate ( ctx ) {
    // only allow to populate if authorized admin
    const { authorization: auth_token, } = ctx.headers;

    if (!auth_token || "1" !== auth_token) {
      ctx.throw(403);
    } else {
      try {
        await populateDatabase();
        ctx.status = 200;
        ctx.body = { ok: true, };
      } catch ( e: unknown ) {
        ctx.body = { ok: false, message: `${e}`, };
        ctx.throw(500);
      }
    }
  });

  app.use(bodyParser());

  app.useRouter(router);
  app.useRouter(apollo.getRouter());

  app.use(async function errorHandler ( ctx, next ) {
    try {
      await next();
    // eslint-disable-next-line @typescript-eslint/no-implicit-any-catch, @typescript-eslint/no-explicit-any
    } catch ( err: any ) {
      ctx.status = err.statusCode || err.status || 500;
      ctx.body = {
        message: err.message,
        ok     : false,
      };
    }
  });

  await apollo.start();
  await app.start({ port: Number(PORT), });

  console.log(app.getServer().address());
})().catch(console.error.bind(console));
